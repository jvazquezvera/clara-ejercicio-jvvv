/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clara.ejercicio.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "employees")
@XmlRootElement
public class Employee implements Serializable {

	private static final long serialVersionUID = 1530028955840490935L;

	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "employee_id")
	private Integer employeeId;
	@Basic(optional = false)
	@NotNull
	@Column(name = "full_name")
	private String fullName;
	@Basic(optional = false)
	@NotNull
	@Column(name = "manager_id")
	private Integer managerId;
	
	
	
	
	public Employee() {
	
	}

	public Integer getEmployeeId() {
		return employeeId;
	}





	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}





	public String getFullName() {
		return fullName;
	}





	public void setFullName(String fullName) {
		this.fullName = fullName;
	}





	public Integer getManagerId() {
		return managerId;
	}





	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}
	
	
	

}
