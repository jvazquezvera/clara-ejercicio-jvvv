/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clara.ejercicio.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "roles")
@XmlRootElement
public class Rol implements Serializable {

	private static final long serialVersionUID = 1530028955840490935L;

	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "role_id")
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Column(name = "role_name")
	private String rolName;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRolName() {
		return rolName;
	}
	public void setRolName(String rolName) {
		this.rolName = rolName;
	}
	
	
}
