/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clara.ejercicio.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "employee_role")
@XmlRootElement
public class EmployeeRole implements Serializable {

	private static final long serialVersionUID = 1530028955840490935L;

	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "role_id")
	private Integer roleId;
	@Basic(optional = false)
	@NotNull
	@Column(name = "employee_id")
	private Integer employeeId;
	
	
	
	
	public EmployeeRole() {
	
	}

	public Integer getEmployeeId() {
		return employeeId;
	}





	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}





	
	

}
