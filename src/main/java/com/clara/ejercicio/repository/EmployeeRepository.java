package com.clara.ejercicio.repository;


import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.clara.ejercicio.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
	
	
	
	@Query(value = "WITH RECURSIVE subordinates AS (\n" + 
			"			SELECT\n" + 
			"				employee_id,\n" + 
			"				manager_id,\n" + 
			"				full_name\n" + 
			"			FROM\n" + 
			"				employees\n" + 
			"			WHERE\n" + 
			"				employee_id = :employeeId\n" + 
			"			UNION\n" + 
			"				SELECT\n" + 
			"					e.employee_id,\n" + 
			"					e.manager_id,\n" + 
			"					e.full_name\n" + 
			"				FROM\n" + 
			"					employees e\n" + 
			"				INNER JOIN subordinates s ON s.employee_id = e.manager_id\n" + 
			"		) select *\n" + 
			"		FROM\n" + 
			"			subordinates;"
			, 
			  nativeQuery = true)
	List<Employee> findByIdRecursivo(Integer employeeId);


	
}
