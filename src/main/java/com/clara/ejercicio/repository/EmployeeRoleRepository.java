package com.clara.ejercicio.repository;


import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.clara.ejercicio.model.EmployeeRole;

public interface EmployeeRoleRepository extends CrudRepository<EmployeeRole, Integer> {
	
	@Query(value = "select role_name from roles r inner join employee_role er on er.role_id = r.role_id where er.employee_id =:employeeId",nativeQuery = true)
    List<String> getRolesByUsers(Integer employeeId);	


}
