package com.clara.ejercicio.controller;

import java.sql.SQLException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.clara.ejercicio.model.Employee;
import com.clara.ejercicio.service.EmployeeService;



@RestController
@RequestMapping("/employee")
public class EmployeeController {
	private static final Logger logger = LogManager.getLogger(EmployeeController.class);

	@Autowired
	@Lazy
	EmployeeService personaService;
	
	
	@GetMapping("/tree/{idEmployee}")
	public List<Employee> getTreeEmployee(
			@PathVariable Integer idEmployee
			) throws SQLException {  
		logger.info("Consultando tree employee");
		return personaService.getTreeEmployee(idEmployee);  
	}
	
	
	
	
}
