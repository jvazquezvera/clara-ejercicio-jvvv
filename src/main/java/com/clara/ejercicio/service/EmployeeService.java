/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clara.ejercicio.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clara.ejercicio.model.Employee;
import com.clara.ejercicio.repository.EmployeeRepository;
import com.clara.ejercicio.repository.EmployeeRoleRepository;


@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeeRoleRepository employeeRoleRepository;
	
	public List<Employee> getTreeEmployee(Integer idEmployee) {
		 
		return employeeRepository.findByIdRecursivo(idEmployee);
		
	}
	
	
	public List<String> getRolByUserId(Integer idEmployee){
		return  employeeRoleRepository.getRolesByUsers(idEmployee);
		
		
	}
	
	
	

	
}
