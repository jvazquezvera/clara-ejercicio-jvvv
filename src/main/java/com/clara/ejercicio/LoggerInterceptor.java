package com.clara.ejercicio;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class LoggerInterceptor implements HandlerInterceptor {
  private static final Logger logger = LogManager.getLogger(LoggerInterceptor.class);

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
    long executionStartTime = System.currentTimeMillis();
    request.setAttribute("executionStartTime", executionStartTime);
    
    String logId = UUID.randomUUID().toString();
    String uriPath = request.getServletPath();
    String userIp = request.getRemoteAddr();
    String localPort = String.valueOf(request.getLocalPort());
    String localHost = "";
	try {
		localHost = InetAddress.getLocalHost().getHostName();
	} catch (UnknownHostException e) {}
    
    ThreadContext.put("logId", logId);
    ThreadContext.put("uriPath", uriPath);
    ThreadContext.put("userIp", userIp);
    ThreadContext.put("localPort", localPort);
	ThreadContext.put("localHost", localHost);
	ThreadContext.put("method", request.getMethod());
    logger.debug("thread=INI");
	return true;
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
   Exception exception) throws Exception {
    long executionStartTime = (Long)request.getAttribute("executionStartTime");
    long executionEndTime = System.currentTimeMillis();
    long executionDuration = executionEndTime - executionStartTime;
    ThreadContext.put("execution-duration", Long.toString(executionDuration));
    logger.info("thread=END");
    ThreadContext.clearMap();
  }
}
